GM = 0
SUBT = 1

STR  =  0
CODE =  1
LINE =  2
COL  =  3

GM_SIGNAL_PROGRAM             = "<signal-program>"
GM_PROGRAM                    = "<program>"
GM_BLOCK                      = "<block>"
GM_DECLARATIONS               = "<declarations>"
GM_CONSTANT_DECLARATIONS      = "<constant-declarations>"
GM_CONSTANT_DECLARATIONS_LIST = "<constant-declarations-list>"
GM_CONSTANT_DECLARATION       = "<constant-declaration>"
GM_STATEMENT_LIST             = "<statements-list>"
GM_STATEMENT                  = "<statement>"
GM_CONSTANT                   = "<constant>"
GM_CONSTANT_IDENTIFIER        = "<constant-identifier>"
GM_VARIABLE_IDENTIFIER        = "<variable-identifier>"
GM_PROCEDURE_IDENTIFIER       = "<procedure-identifier>"
GM_IDENTIFIER                 = "<identifier>"
GM_UNSIGNED_INTEGER           = "<unsigned-integer>"
GM_EMPTY                      = "<empty>"

def __gn_generate(tree, constants, identifiers, names, var, errors, outcode):
    node = tree[GM]
    if str != type(node):
        return None
    if GM_SIGNAL_PROGRAM == node:
        return __gn_generate(tree[SUBT], constants, identifiers, names, var, errors, outcode)
    elif GM_PROGRAM == node:
        bf = __gn_generate(tree[SUBT][1:3], constants, identifiers, names, var, errors, outcode)
        names[bf[STR]] = (bf[LINE], bf[COL])
        outcode.write("section .text\n")
        outcode.write("  global _" + bf[STR] + '\n\n')
        outcode.write("_" + bf[STR] + ':\n')
        return __gn_generate(tree[SUBT][4:6], constants, identifiers, names, var, errors, outcode)
    elif GM_BLOCK == node:
        (names, var, errors) = __gn_generate(tree[SUBT][3:5], constants, identifiers, names, var, errors, outcode)
        outcode.write("\nsection .data\n")
        return __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)
    elif GM_DECLARATIONS == node:
        return __gn_generate(tree[SUBT], constants, identifiers, names, var, errors, outcode)
    elif GM_CONSTANT_DECLARATIONS == node:
        if GM_EMPTY == tree[SUBT][0]:
            return (names, var, errors)
        return __gn_generate(tree[SUBT][1:3], constants, identifiers, names, var, errors, outcode)
    elif GM_CONSTANT_DECLARATIONS_LIST == node:
        if GM_EMPTY == tree[SUBT][0]:
            return (names, var, errors)
        (names, var, errors) = __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)
        return __gn_generate(tree[SUBT][2:4], constants, identifiers, names, var, errors, outcode)
    elif GM_CONSTANT_DECLARATION == node:
        bf = __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)
        if bf[STR] in var.keys():
            errors.append("Constant can not changing: line {}; col {}".format(bf[LINE], bf[COL]))
            errors.append("first used: line {}; col {}".format(var[bf[STR]][0], var[bf[STR]][1]))
            return (names, var, errors)
        if bf[STR] in names.keys():
            errors.append("Program can not have name that already used: line {}; col {}".format(bf[LINE], bf[COL]))
            errors.append("first used: line {}; col {}".format(names[bf[STR]][0], names[bf[STR]][1]))
            return (names, var, errors)
        var[bf[STR]] = (bf[LINE], bf[COL])
        con = __gn_generate(tree[SUBT][3:5], constants, identifiers, names, var, errors, outcode)
        outcode.write(bf[STR] + " equ " + con + '\n')
        return (names, var, errors)
    elif GM_STATEMENT_LIST == node:
        if GM_EMPTY == tree[SUBT][0]:
            return (names, var, errors)
        (names, var, errors) = __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)
        return __gn_generate(tree[SUBT][2:4], constants, identifiers, names, var, errors, outcode)
    elif GM_STATEMENT == node:
        (bf, _type) = __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)
        if bf[STR] not in var.keys():
            var[bf[STR]] = (bf[LINE], bf[COL])
        if bf[STR] in names.keys():
            errors.append("Program can not have name that already used: line {}; col {}".format(bf[LINE], bf[COL]))
            errors.append("first used: line {}; col {}".format(names[bf[STR]][0], names[bf[STR]][1]))
            return (names, var, errors)
        con = __gn_generate(tree[SUBT][3:5], constants, identifiers, names, var, errors, outcode)
        if ('-' == con[0] and 'NATINT' == _type):
            errors.append("Can not assign signed to natural: line {}; col {}".format(bf[LINE], bf[COL]))
            return (names, var, errors)
        outcode.write("  mov " + bf[STR] + ", " + con + '\n')
        return (names, var, errors)
    elif GM_CONSTANT == node:
        if 2 < len(tree[SUBT]):
            return '-' + __gn_generate(tree[SUBT][1:3], constants, identifiers, names, var, errors, outcode)[STR]
        else:
            return __gn_generate(tree[SUBT][0:2], constants, identifiers, names, var, errors, outcode)[STR]
    elif GM_CONSTANT_IDENTIFIER == node:
        return __gn_generate(tree[SUBT], constants, identifiers, names, var, errors, outcode)
    elif GM_VARIABLE_IDENTIFIER == node:
        return (__gn_generate(tree[SUBT], constants, identifiers, names, var, errors, outcode), "NATINT")
    elif GM_PROCEDURE_IDENTIFIER == node:
        return __gn_generate(tree[SUBT], constants, identifiers, names, var, errors, outcode)
    elif GM_IDENTIFIER == node:
        return tree[SUBT][0]
    elif GM_UNSIGNED_INTEGER == node:
        return tree[SUBT][0]
    else:
        return None

def generator(args, outcode):
    if (None != args):
        return __gn_generate(args[0], args[1], args[2], {}, {}, [], outcode)
    return None
