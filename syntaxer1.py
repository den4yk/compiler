STR  =  0
CODE =  1
LINE =  2
COL  =  3
LAST = -1
KW_PROGRAM = 401
KW_BEGIN   = 402
KW_END     = 403
KW_CONST   = 404
KW_IF      = 405
KW_THEN    = 406
KW_ELSE    = 407
DM_SEMICOLON = 59
DM_DOT       = 46
DM_EQUAL     = 61
DM_NEGATE    = 45
DM_LESS      = 60
DM_GREATER   = 62
DM_ASSIGN    = 301


def __sx_print_tree(tree, tabs):
    if None != tree:
        for elem in tree:
            if (list == type(elem)):
                __sx_print_tree(elem, tabs + '  ')
            elif (str == type(elem)):
                print tabs + elem
            else:
                print tabs + str(elem[STR])
    return tree


def __sx_identifier(lexem, identifiers):
    if (lexem[STR] in identifiers.keys()):
        retval = ['<identifier>', [lexem]]
        return retval
    return None


def __sx_constant(lexem, constants):
    if (lexem[STR] in constants.keys()):
        retval = ['<unsigned-integer>', [lexem]]
        return retval
    #print "Unexpected token {}: line {}; col {}".format(lexem[STR], lexem[LINE], lexem[COL])
    return None


def __sx_declaration(lexems, constants, identifiers):
    retval = []
#<constant-identifier>
    current = -len(lexems)
    if (0 == current):
        return ['<empty>']
    res = __sx_identifier(lexems[current], identifiers)
    if (None == res):
        return ['<empty>']
    retval.append('<constant-identifier>')
    retval.append(res)
#=
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (DM_EQUAL != lexems[current][CODE]):
        print "Unexpected token {}, '=' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
#<constant>
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    retval.append('<constant>')
    minus = None
    if (DM_NEGATE == lexems[current][CODE]):
        minus = [lexems[current]]
        current += 1
    res = __sx_constant(lexems[current], constants)
    if (None == res):
        print "Constant expected, found {}: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    if None == minus:
        retval.append(res)
    else:
        retval.append(minus + res)
#;
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (DM_SEMICOLON != lexems[current][CODE]):
        print "Unexpected token {}, ';' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
    retval = ['<constant-declaration>'] + [retval]
    return (retval, minus)


def __sx_declarations(lexems, constants, identifiers):
    retval = []
#<constant-declaration>
    current = -len(lexems)
    if (0 == current):
        return (['<constant-declarations-list>',['<empty>']], current)
    res = __sx_declaration(lexems[current:], constants, identifiers)
    if (['<empty>'] == res):
        return (['<constant-declarations-list>',['<empty>']], current)
    if (None == res):
        return (None, current)
    (res, minus) = res
    retval += res
#<constant-declarations-list>
    if None == minus:
        current += 4
    else:
        current += 5
    if (0 <= current):
        retval = ['<constant-declarations-list>'] + [retval] + [['<constant-declarations-list>',['<empty>']]]
        return (retval, 0)
    res, current = __sx_declarations(lexems[current:], constants, identifiers)
    if (res == None):
        return (None, current)
    retval += res
    retval = ['<constant-declarations-list>'] + [retval]
    return (retval, current)


def __sx_constdeclarations(lexems, constants, identifiers):
    retval = []
#CONST
    current = -len(lexems)
    if (0 == current or KW_CONST != lexems[current][CODE]):
        return (['<empty>'], current)
    retval.append(lexems[current])
#<constant-declarations-list>
    current += 1
    res, current = __sx_declarations(lexems[current:], constants, identifiers)
    if (None == res):
        return (None, current)
    retval += res
    retval = ['<constant-declarations>'] + [retval]
    return (retval, current)


def __sx_statement(lexems, constants, identifiers):
    retval = []
#<variable-identifier>
    current = -len(lexems)
    if (0 == current):
        return ['<empty>']
    res = __sx_identifier(lexems[current], identifiers)
    if (None == res):
        print "Identifier expected, found {}: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append('<variable-identifier>')
    retval.append(res)
#:=
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (DM_ASSIGN != lexems[current][CODE]):
        print "Unexpected token {}, ':=' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
#<constant>
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    retval.append('<constant>')
    minus = None
    if (DM_NEGATE == lexems[current][CODE]):
        minus = [lexems[current]]
        current += 1
    res = __sx_constant(lexems[current], constants)
    if (None == res):
        print "Constant expected, found {}: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    if None == minus:
        retval.append(res)
    else:
        retval.append(minus + res)
#;
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (DM_SEMICOLON != lexems[current][CODE]):
        print "Unexpected token {}, ';' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
    retval = ['<statement>'] + [retval]
    return (retval, minus)


def __sx_statements(lexems, constants, identifiers):
    retval = []
#<statement>
    current = -len(lexems)
    if (0 == current):
        return ['<statements-list>',['<empty>']]
    res = __sx_statement(lexems[current:], constants, identifiers)
    if (['<empty>'] == res):
        return ['<statements-list>',['<empty>']]
    if (None == res):
        return None
    (res, minus) = res
    retval += res
#<statement-list>
    if None == minus:
        current += 4
    else:
        current += 5
    if (0 <= current):
        retval = ['<statements-list>'] + [retval + ['<statements-list>',['<empty>']]]
        return retval
    res = __sx_statements(lexems[current:], constants, identifiers)
    if (res == None):
        return None
    retval += res
    retval = ['<statements-list>'] + [retval]
    return retval


def __sx_block(lexems, constants, identifiers):
    retval = []
#<declarations>
    retval.append('<declarations>')
    res, current = __sx_constdeclarations(lexems, constants, identifiers)
    if (None == res):
        return None
    retval.append(res)
#BEGIN
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (KW_BEGIN != lexems[current][CODE]):
        print "Unexpected token {}, keyword 'BEGIN' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
#<statements-list>
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    res = __sx_statements(lexems[current:LAST], constants, identifiers)
    if (None == res):
        return None
    if ([] != res):
        retval += res
#END
    current = LAST
    if (KW_END != lexems[current][CODE]):
        print "Unexpected token {}, keyword 'END' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
    retval = ['<block>'] + [retval]
    return retval


def __sx_program(lexems, constants, identifiers):
    retval = []
#PROGRAM
    current = -len(lexems)
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (KW_PROGRAM != lexems[current][CODE]):
        print "Unexpected token {}, keyword 'PROGRAM' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
#<procedure-identifier>
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    res = __sx_identifier(lexems[current], identifiers)
    if (None == res):
        print "Identifier expected, found {}: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append('<procedure-identifier>')
    retval.append(res)
#;
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    if (DM_SEMICOLON != lexems[current][CODE]):
        print "Unexpected token {}, ';' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
#<block>
    current += 1
    if (0 == current):
        print 'Unexpected end of file'
        return None
    res = __sx_block(lexems[current:LAST], constants, identifiers)
    if (None == res):
        return None
    retval += res
#.
    current = LAST
    if (DM_DOT != lexems[current][CODE]):
        print "Unexpected token {}, '.' is missed: line {}; col {}".format(lexems[current][STR], lexems[current][LINE], lexems[current][COL])
        return None
    retval.append(lexems[current])
    retval = ['<program>'] + [retval]
    return retval


def syntaxer(args):
    if (None != args):
        retval = []
        retval.append('<signal-program>')
        res = __sx_program(args[0], args[1], args[2])
        if (None != res):
            retval.append(res)
            return (__sx_print_tree(retval, ""), args[1], args[2])
        return None
    return None
