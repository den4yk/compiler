def __lx_print_list(lst):
    for elem in lst:
        print elem
    print '\n'

def __lx_print_dict(dct):
    for elem in dct.keys():
        print "{} : {}".format(elem, dct[elem])
    print '\n'

def lexer(code, keywords, delimeters):
    constants = dict()
    identifiers = dict()
    ident_code = 1001
    cnt_code = 501
    out_lexems = []
    err_list = []
    col = 0
    line = 1
    c = code.read(1)
    while ("" != c):
        if (c.isalpha()):
            ident = c
            ident_col = col
            c = code.read(1)
            while (c.isalpha() or c.isdigit()):
                ident += c
                col += 1
                c = code.read(1)
            if (ident in keywords.keys()):
                out_lexems.append((ident, keywords[ident], line, ident_col))
            elif (ident in identifiers.keys()):
                out_lexems.append((ident, identifiers[ident], line, ident_col))
            else:
                out_lexems.append((ident, ident_code, line, ident_col))
                identifiers[ident] = ident_code
                ident_code += 1
        elif (c.isdigit()):
            cnt = c
            cnt_col = col
            c = code.read(1)
            while (c.isdigit()):
                cnt += c
                col += 1
                c = code.read(1)
            if (c.isalpha()):
                err_list.append('Identifier can not start with number: line {}; col {}'.format(line, cnt_col))
                col += 1
                c = code.read(1)
                while (c.isalpha()):
                    col += 1
                    c = code.read(1)
            else:
                if (cnt in constants.keys()):
                    out_lexems.append((cnt, constants[cnt], line, cnt_col))
                else:
                    out_lexems.append((cnt, cnt_code, line, cnt_col))
                    constants[cnt] = cnt_code
                    cnt_code += 1
        elif ('\n' == c):
            line += 1
            col = -1
            c = code.read(1)
        elif (c.isspace()):
            c = code.read(1)
        elif ('(' == c):
            c = code.read(1)
            comm_col = col
            comm_line = line
            if ('*' == c):
                col += 1
                c = code.read(1)
                while (True):
                    while (("" != c) and ('*' != c)):
                        if ('\n' == c):
                            line += 1
                            col = -1
                        col += 1
                        c = code.read(1)
                    if ("" == c):
                        err_list.append('Unclosed comment: line {}; col {}'.format(comm_line, comm_col))
                        break
                    c = code.read(1)
                    col += 1
                    if (')' == c):
                        c = code.read(1)
                        break
            else:
                err_list.append('Unexpected symbol {}: line {}; col {}'.format('(', line, col))
        elif (':' == c):
            c = code.read(1)
            if ('=' == c):
                out_lexems.append((":=", delimeters[":="], line, col))
                col += 1
                c = code.read(1)
            else:
                err_list.append('Unexpected symbol :: line {}; col {}'.format(c, line, col))
                
        elif (c in delimeters.keys()):
            out_lexems.append((c, delimeters[c], line, col))
            c = code.read(1)
        else:
            err_list.append('Unexpected symbol {}: line {}; col {}'.format(c, line, col))
            c = code.read(1)
        col += 1
    if (0 == len(err_list)):
        __lx_print_list(out_lexems)
        __lx_print_dict(constants)
        __lx_print_dict(identifiers)
        return (out_lexems, constants, identifiers)
    for err in err_list:
        print err
    return None
