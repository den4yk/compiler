from lexer import lexer
from syntaxer1 import syntaxer
from generator import generator
from sys import argv
from os.path import basename, splitext

CODE = 1
TOKEN = 0

def makeofname(fname):
    return splitext(basename(fname))[0] + '.asm'

def load(filename):
    res = open(filename).readlines()
    res = dict(map(lambda x: (x.split(' ')[TOKEN], int(x.split(' ')[CODE])), res))
    return res

if __name__ == "__main__":
    fname = "code.txt" if 1 == len(argv) else argv[1]
    code = open(fname)
    outcode = open(makeofname(fname), "w")
    keywords = load("keywords.txt")
    delimeters = load("delimeters.txt")
    res = generator(syntaxer(lexer(code, keywords, delimeters)), outcode)
    print res[0]
    print res[1]
    if ([] != res[2]):
        for err in res[2]:
            print err
    code.close()
    outcode.close()
